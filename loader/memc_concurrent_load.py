#!/usr/bin/env python
# -*- coding: utf-8 -*-
import collections
import glob
import gzip
import logging
import os
import signal
import sys
import threading
import time
from functools import partial
from multiprocessing import Pool, cpu_count
from optparse import OptionParser
import Queue

# pip install python-memcached
import memcache

# brew install protobuf
# protoc  --python_out=. ./appsinstalled.protofiles
# pip install protobuf
import appsinstalled_pb2

"""
Третий вариант решения. Первые два есть в истории коммитов, но лучше туда не смотреть ))

количество строк в файлах:

$ zcat files/20170929000000.tsv.gz | wc -l
3422995
$ zcat files/20170929000100.tsv.gz | wc -l
3424477
$ zcat files/20170929000200.tsv.gz | wc -l
3422026

совпадает с количеством обработанных строк:

[2018.12.02 23:14:23] I Processed 3422026 records, acceptable error rate (0.0). Successfull load
[2018.12.02 23:14:24] I Processed 3424477 records, acceptable error rate (0.0). Successfull load
[2018.12.02 23:14:24] I Processed 3422995 records, acceptable error rate (0.0). Successfull load

в мемкеше получилось вот такое:

stats cachedump 8 20
ITEM dvid:95f30294526bee88155a5abb79aaf7ac [297 b; 0 s]
ITEM gaid:54b65cc5b97d2f279f352f3f04e4b3bd [312 b; 0 s]
ITEM idfa:c2bccdd590d5f7ea515c87c49c3c4c6e [311 b; 0 s]
ITEM dvid:8b2db36e7dec9da38540e98657b4117c [312 b; 0 s]
ITEM gaid:9cd387a3cb9a1c5bc46fd6abd742dcad [315 b; 0 s]
ITEM gaid:591e944ce28440846e62bc704feaf7a0 [317 b; 0 s]
ITEM adid:94852bad0ae664393a01d3c4dc6f874e [302 b; 0 s]
ITEM gaid:44b823811288d566fd5ec5a4aa7eec2d [302 b; 0 s]
ITEM dvid:9f655ac43dc22a3e6ac8809cda705822 [300 b; 0 s]
ITEM adid:156ae77cbd09f4cee158898586500c99 [305 b; 0 s]
ITEM dvid:661fdc735a7aaf653637263ef49b3de2 [315 b; 0 s]
ITEM gaid:52f5ebae214ab7ada7ab4c31cd27eece [290 b; 0 s]
ITEM adid:65b45e340fbd932bade5fe4dfb4b29d9 [314 b; 0 s]
ITEM gaid:3783ea36dafe24dadb2fa5ee9efdadb1 [298 b; 0 s]
ITEM idfa:4929e7cf291ebb5eea3569cfee370cf2 [296 b; 0 s]
ITEM adid:affa5e3fc70b04202b99e1a2a8d377f7 [308 b; 0 s]
ITEM dvid:e7c4d45664c46242e2c8262510b00a44 [305 b; 0 s]
ITEM gaid:51886d42872a2b4722b6eb0138a0473f [294 b; 0 s]
ITEM idfa:89499af4b1a889ebc17374922afa8486 [303 b; 0 s]
ITEM idfa:6ec588ade3c574efb19f4843921dffb3 [299 b; 0 s]
END

"""

AppsInstalled = collections.namedtuple("AppsInstalled", ["dev_type", "dev_id", "lat", "lon", "apps"])

config = {
    'MEMC_RETRIES': 3,
    'MEMC_TIMEOUT': 1,
    "MEMC_BACKOFF": 0.1,
    'JOBS_QUEUE_SIZE': 1000,
    'RESULTS_QUEUE_SIZE': 0,
    'NORMAL_ERR_RATE': 0.01,
    'THREADS': 4,
}


def pack_into_protobuf(appsinstalled):
    """
    :param appsinstalled:
    :return:
    """

    ua = appsinstalled_pb2.UserApps()
    ua.lat = appsinstalled.lat
    ua.lon = appsinstalled.lon
    key = "%s:%s" % (appsinstalled.dev_type, appsinstalled.dev_id)
    ua.apps.extend(appsinstalled.apps)
    packed = ua.SerializeToString()
    return key, packed, ua


def create_memc_clients(device_memc, timeout, dry):
    clients = {}
    for device, memc_address in device_memc.iteritems():
        if not dry:
            clients[device] = memcache.Client([memc_address], socket_timeout=timeout)
        else:
            clients[device] = None
    return clients


def dot_rename(path):
    head, fn = os.path.split(path)
    os.rename(path, os.path.join(head, "." + fn))


def insert_appsinstalled(memc_client, memc_addr, appsinstalled, retries, backoff, dry):

    key, packed, ua = pack_into_protobuf(appsinstalled)
    if dry:
        logging.debug("%s - %s -> %s" % (memc_addr, key, str(ua).replace("\n", " ")))
        return True
    for attempt in range(retries):
        success = memc_client.set(key, packed)
        if success:
            return True
        else:
            logging.error("Cannot write in memcached: {} - {} -> {}".format(
                memc_addr, key, str(ua).replace("\n", " "))
            )
            delay = backoff * (2 ** attempt)
            logging.info("Retry in {} seconds".format(delay))
            time.sleep(delay)
    else:
        return False


def parse_appsinstalled(line):
    line_parts = line.strip().split("\t")
    if len(line_parts) < 5:
        return
    dev_type, dev_id, lat, lon, raw_apps = line_parts
    if not dev_type or not dev_id:
        return
    try:
        apps = [int(a.strip()) for a in raw_apps.split(",")]
    except ValueError:
        apps = [int(a.strip()) for a in raw_apps.split(",") if a.isidigit()]
        logging.info("Not all user apps are digits: `%s`" % line)
    try:
        lat, lon = float(lat), float(lon)
    except ValueError:
        logging.info("Invalid geo coords: `%s`" % line)
    return AppsInstalled(dev_type, dev_id, lat, lon, apps)


def record_handler(jobs, results, sentinel, retries, backoff):
    success, errors = 0, 0

    while sentinel['run']:
        try:
            task = jobs.get(timeout=0.1)
        except Queue.Empty:
            results.put((success, errors))
            return

        memc_client, memc_address, appsinstalled, dry = task
        ok = insert_appsinstalled(memc_client, memc_address, appsinstalled, retries, backoff, dry)
        if ok:
            success += 1
        else:
            errors += 1


def sigint_handler(sentinel, results):
    def handler(signum, frame):
        sentinel['run'] = False
        results.close()
    return handler


def process_worker(fn, options, config):
    device_memc = {
        "idfa": options.idfa,
        "gaid": options.gaid,
        "adid": options.adid,
        "dvid": options.dvid,
    }

    memc_clients = create_memc_clients(device_memc, config['MEMC_TIMEOUT'], options.dry)

    jobs, results = Queue.Queue(config['JOBS_QUEUE_SIZE']), Queue.Queue(['RESULTS_QUEUE_SIZE'])
    sentinel = {'run': True}
    signal.signal(signal.SIGINT, sigint_handler(sentinel, results))

    threads = set()
    for i in range(config['THREADS']):
        thread = threading.Thread(target=record_handler, args=(
            jobs, results, sentinel, config['MEMC_RETRIES'], config['MEMC_BACKOFF']
        ))
        thread.daemon = True
        threads.add(thread)

    for thread in threads:
        thread.start()

    success, errors = 0, 0

    logging.info('Processing %s' % fn)
    with gzip.open(fn) as logs:
        for line in logs:
            if not line.strip():
                continue

            appsinstalled = parse_appsinstalled(line)
            if not appsinstalled:
                errors += 1
                continue
            memc_client = memc_clients.get(appsinstalled.dev_type)
            memc_addr = device_memc.get(appsinstalled.dev_type)
            if not memc_addr:
                errors += 1
                logging.error("Unknow device type: %s" % appsinstalled.dev_type)
                continue
            jobs.put((memc_client, memc_addr, appsinstalled, options.dry))
            if not all(thread.is_alive() for thread in threads):
                break

    for thread in threads:
        if thread.is_alive():
            thread.join()

    while not results.empty():
        success_in_thread, errors_in_thread = results.get()
        success += success_in_thread
        errors += errors_in_thread
    total = success + errors
    err_rate = float(errors) / success if success else 1

    if err_rate < config['NORMAL_ERR_RATE']:
        logging.info("Processed {} records, acceptable "
                     "error rate ({}). Successfull load".format(total, err_rate))
    else:
        logging.error("High error rate ({} > {}). Failed load".format(err_rate, config['NORMAL_ERR_RATE']))

    return fn


def main(options):
    files = sorted(glob.iglob(options.pattern))
    if not files:
        logging.info("Nothing to do here")
        return
    concurrency = min(cpu_count(), len(files))
    logging.info("Creating {} processes".format(concurrency))
    pool = Pool(processes=concurrency)
    worker = partial(process_worker, options=options, config=config)
    try:
        for file_name in pool.imap(worker, files):
            logging.info("Renaming {}".format(file_name))
            dot_rename(file_name)
    except KeyboardInterrupt:
        logging.info("Cancelling...")
        pool.terminate()


def prototest():
    sample = "idfa\t1rfw452y52g2gq4g\t55.55\t42.42\t1423,43,567,3,7,23\ngaid\t7rfw452y52g2gq4g\t55.55\t42.42\t7423,424"
    for line in sample.splitlines():
        dev_type, dev_id, lat, lon, raw_apps = line.strip().split("\t")
        apps = [int(a) for a in raw_apps.split(",") if a.isdigit()]
        lat, lon = float(lat), float(lon)
        ua = appsinstalled_pb2.UserApps()
        ua.lat = lat
        ua.lon = lon
        ua.apps.extend(apps)
        packed = ua.SerializeToString()
        unpacked = appsinstalled_pb2.UserApps()
        unpacked.ParseFromString(packed)
        assert ua == unpacked


if __name__ == '__main__':
    op = OptionParser()
    op.add_option("-t", "--test", action="store_true", default=False)
    op.add_option("-l", "--log", action="store", default=None)
    op.add_option("--dry", action="store_true", default=False)
    op.add_option("--pattern", action="store", default="/data/appsinstalled/*.tsv.gz")
    op.add_option("--idfa", action="store", default="127.0.0.1:33013")
    op.add_option("--gaid", action="store", default="127.0.0.1:33014")
    op.add_option("--adid", action="store", default="127.0.0.1:33015")
    op.add_option("--dvid", action="store", default="127.0.0.1:33016")
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO if not opts.dry else logging.DEBUG,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    if opts.test:
        prototest()
        sys.exit(0)

    logging.info("Memc loader started with options: %s" % opts)
    try:
        main(opts)

    except Exception as e:
        logging.exception("Unexpected error: %s" % e)
        sys.exit(1)
